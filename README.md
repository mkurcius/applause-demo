# Demo

## Build requirements
1. JDK 8
2. Maven
3. NodeJS 12+


## Run server (*applause-demo-server*)
`mvn spring-boot:run`


## Run client (*applause-demo-client*)
1. `npm ci`
2. `npm start`

Open `http://localhost:4200` in the browser
