package com.example.demo.api.search;

import com.example.demo.api.search.dto.CriteriaDTO;
import com.example.demo.api.search.dto.CriteriaValuesDTO;
import com.example.demo.api.search.dto.DeviceDTO;
import com.example.demo.api.search.dto.TesterDTO;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TesterController.class)
class TesterControllerTest {
  @Autowired
  private MockMvc mvc;

  @MockBean
  private TesterDAO testerDAO;

  @Captor
  private ArgumentCaptor<CriteriaDTO> criteriaCaptor;

  @Test
  public void shouldReturnCriteriaValues() throws Exception {
    CriteriaValuesDTO values = CriteriaValuesDTO.builder()
        .countries(ImmutableList.of("US", "PL"))
        .devices(ImmutableList.of(new DeviceDTO(1L, "Device 1"), new DeviceDTO(2L, "Device 2")))
        .build();

    given(testerDAO.getCriteriaValues()).willReturn(values);

    mvc.perform(get("/api/testers/criteria").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().json(
            "{" +
            "  \"countries\": [" +
            "    \"US\"," +
            "    \"PL\"" +
            "  ]," +
            "  \"devices\": [" +
            "    {" +
            "      \"id\": 1," +
            "      \"label\": \"Device 1\"" +
            "    }," +
            "    {" +
            "      \"id\": 2," +
            "      \"label\": \"Device 2\"" +
            "    }" +
            "  ]" +
            "}"));
  }


  @Test
  public void shouldFindTestersByCriteria() throws Exception {
    List<TesterDTO> testers = ImmutableList.of(
        new TesterDTO(1L, "Monkey D.", "Luffy", 100L),
        new TesterDTO(2L, "Roronoa", "Zoro", 87L),
        new TesterDTO(3L, "Portgas D.", "Ace", 50L));

    given(testerDAO.findTesters(any(CriteriaDTO.class))).willReturn(testers);

    mvc.perform(get("/api/testers")
        .param("country", "JP")
        .param("device", "10")
        .param("device", "20")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().json(
            "[" +
            "  {" +
            "    \"id\": 1," +
            "    \"firstName\": \"Monkey D.\"," +
            "    \"lastName\": \"Luffy\"," +
            "    \"rank\": 100" +
            "  }," +
            "  {" +
            "    \"id\": 2," +
            "    \"firstName\": \"Roronoa\"," +
            "    \"lastName\": \"Zoro\"," +
            "    \"rank\": 87" +
            "  }," +
            "  {" +
            "    \"id\": 3," +
            "    \"firstName\": \"Portgas D.\"," +
            "    \"lastName\": \"Ace\"," +
            "    \"rank\": 50" +
            "  }" +
            "]"));

    verify(testerDAO).findTesters(criteriaCaptor.capture());

    assertThat(criteriaCaptor.getValue())
        .usingRecursiveComparison()
        .isEqualTo(new CriteriaDTO(ImmutableSet.of("JP"), ImmutableSet.of(10L, 20L)));
  }

  @Test
  public void shouldFindTestersWithoutCriteria() throws Exception {
    List<TesterDTO> testers = ImmutableList.of(
        new TesterDTO(1L, "Monkey D.", "Luffy", 100L),
        new TesterDTO(2L, "Roronoa", "Zoro", 87L),
        new TesterDTO(3L, "Portgas D.", "Ace", 50L));

    given(testerDAO.findTesters(any(CriteriaDTO.class))).willReturn(testers);

    mvc.perform(get("/api/testers")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().json(
            "[" +
            "  {" +
            "    \"id\": 1," +
            "    \"firstName\": \"Monkey D.\"," +
            "    \"lastName\": \"Luffy\"," +
            "    \"rank\": 100" +
            "  }," +
            "  {" +
            "    \"id\": 2," +
            "    \"firstName\": \"Roronoa\"," +
            "    \"lastName\": \"Zoro\"," +
            "    \"rank\": 87" +
            "  }," +
            "  {" +
            "    \"id\": 3," +
            "    \"firstName\": \"Portgas D.\"," +
            "    \"lastName\": \"Ace\"," +
            "    \"rank\": 50" +
            "  }" +
            "]"));

    verify(testerDAO).findTesters(criteriaCaptor.capture());

    assertThat(criteriaCaptor.getValue())
        .usingRecursiveComparison()
        .isEqualTo(new CriteriaDTO(ImmutableSet.of(), ImmutableSet.of()));
  }
}
