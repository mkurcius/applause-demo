package com.example.demo.api.search;

import com.example.demo.api.search.dto.CriteriaDTO;
import com.example.demo.api.search.dto.CriteriaValuesDTO;
import com.example.demo.api.search.dto.DeviceDTO;
import com.example.demo.api.search.dto.TesterDTO;
import com.example.demo.domain.Bug;
import com.example.demo.domain.Device;
import com.example.demo.domain.Tester;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@Transactional
@SpringBootTest
@AutoConfigureTestEntityManager
class TesterDAOTest {
  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private TesterDAO sut;

  private static final long ANIMUS_ID = 100L;
  private static final long APPLE_OF_EDEN_ID = 200L;

  @BeforeEach
  void setUp() {
    Device animus = entityManager.persist(new Device().setId(ANIMUS_ID).setDescription("Animus"));
    Device appleOfEden = entityManager.persist(new Device().setId(APPLE_OF_EDEN_ID).setDescription("Apple of Eden"));

    entityManager.persist(new Tester()
        .setId(10L)
        .setFirstName("Altaïr")
        .setLastName("Ibn-LaʼAhad")
        .setCountry("SY")
        .addDevice(appleOfEden)
        .addBug(new Bug().setId(101L).setDevice(appleOfEden)));

    entityManager.persist(new Tester()
        .setId(20L)
        .setFirstName("Ezio")
        .setLastName("Auditore da Firenze")
        .setCountry("IT")
        .addDevice(appleOfEden));

    entityManager.persist(new Tester()
        .setId(30L)
        .setFirstName("Desmond")
        .setLastName("Miles")
        .setCountry("US")
        .addDevice(animus)
        .addDevice(appleOfEden)
        .addBug(new Bug().setId(301L).setDevice(animus))
        .addBug(new Bug().setId(302L).setDevice(animus))
        .addBug(new Bug().setId(303L).setDevice(animus))
        .addBug(new Bug().setId(304L).setDevice(appleOfEden)));

    entityManager.persist(new Tester()
        .setId(40L)
        .setFirstName("Lucy")
        .setLastName("Stillman")
        .setCountry("US")
        .addDevice(animus)
        .addBug(new Bug().setId(401L).setDevice(animus))
        .addBug(new Bug().setId(402L).setDevice(animus)));
  }


  @Test
  public void shouldReturnCriteriaValues() {
    CriteriaValuesDTO result = sut.getCriteriaValues();
    assertThat(result.getCountries()).containsOnly("US", "SY", "IT");
    assertThat(result.getDevices()).containsOnly(
        new DeviceDTO(100L, "Animus"),
        new DeviceDTO(200L, "Apple of Eden"));
  }

  @ParameterizedTest
  @ArgumentsSource(FindTesterTestParams.class)
  public void shouldFindTesters(CriteriaDTO criteriaDTO, TesterDTO[] expected) {
    List<TesterDTO> result = sut.findTesters(criteriaDTO);

    assertThat(result)
        .usingRecursiveFieldByFieldElementComparator()
        .containsExactly(expected);
  }

  public static class FindTesterTestParams implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
      return Stream.of(
          arguments(
              new CriteriaDTO(null, null),
              new TesterDTO[]{
                  new TesterDTO(30L, "Desmond", "Miles", 4),
                  new TesterDTO(40L, "Lucy", "Stillman", 2),
                  new TesterDTO(10L, "Altaïr", "Ibn-LaʼAhad", 1),
                  new TesterDTO(20L, "Ezio", "Auditore da Firenze", 0),
              }),
          arguments(
              new CriteriaDTO(ImmutableSet.of("SY"), null),
              new TesterDTO[]{
                  new TesterDTO(10L, "Altaïr", "Ibn-LaʼAhad", 1),
              }),
          arguments(
              new CriteriaDTO(ImmutableSet.of("SY", "US"), null),
              new TesterDTO[]{
                  new TesterDTO(30L, "Desmond", "Miles", 4),
                  new TesterDTO(40L, "Lucy", "Stillman", 2),
                  new TesterDTO(10L, "Altaïr", "Ibn-LaʼAhad", 1),
              }),
          arguments(
              new CriteriaDTO(ImmutableSet.of("SY", "US"), ImmutableSet.of(ANIMUS_ID)),
              new TesterDTO[]{
                  new TesterDTO(30L, "Desmond", "Miles", 4),
                  new TesterDTO(40L, "Lucy", "Stillman", 2),
              }),
          arguments(
              new CriteriaDTO(null, ImmutableSet.of(APPLE_OF_EDEN_ID)),
              new TesterDTO[]{
                  new TesterDTO(30L, "Desmond", "Miles", 4),
                  new TesterDTO(10L, "Altaïr", "Ibn-LaʼAhad", 1),
                  new TesterDTO(20L, "Ezio", "Auditore da Firenze", 0),
              }),
          arguments(
              new CriteriaDTO(null, ImmutableSet.of(ANIMUS_ID, APPLE_OF_EDEN_ID)),
              new TesterDTO[]{
                  new TesterDTO(30L, "Desmond", "Miles", 4),
                  new TesterDTO(40L, "Lucy", "Stillman", 2),
                  new TesterDTO(10L, "Altaïr", "Ibn-LaʼAhad", 1),
                  new TesterDTO(20L, "Ezio", "Auditore da Firenze", 0),
              }));
    }
  }
}
