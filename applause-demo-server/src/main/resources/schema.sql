CREATE TABLE tbl_tester
(
  id         long PRIMARY KEY,
  first_name text NOT NULL,
  last_name  text NOT NULL,
  country    text NOT NULL,
  last_login timestamp
);

CREATE TABLE tbl_device
(
  id          long PRIMARY KEY,
  description text NOT NULL
);

CREATE TABLE tbl_tester_device
(
  tester_id long REFERENCES tbl_tester (id) ON UPDATE CASCADE,
  device_id long REFERENCES tbl_device (id) ON UPDATE CASCADE,

  PRIMARY KEY (tester_id, device_id)
);

CREATE TABLE tbl_bug
(
  id        long PRIMARY KEY,
  device_id long REFERENCES tbl_device (id) ON UPDATE CASCADE,
  tester_id long REFERENCES tbl_tester (id) ON UPDATE CASCADE
);
