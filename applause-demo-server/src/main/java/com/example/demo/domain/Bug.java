package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "tbl_bug")
public class Bug {
  @Id
  private Long id;

  @ManyToOne(optional = false)
  @JoinColumn(name = "tester_id")
  private Tester tester;

  @ManyToOne(optional = false)
  @JoinColumn(name = "device_id")
  private Device device;
}
