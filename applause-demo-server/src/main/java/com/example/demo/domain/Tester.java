package com.example.demo.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "tbl_tester")
public class Tester {
  @Id
  private Long id;

  @Column(nullable = false)
  private String firstName;
  @Column(nullable = false)
  private String lastName;
  @Column(nullable = false)
  private String country;

  private ZonedDateTime lastLogin;

  @Setter(AccessLevel.NONE)
  @ManyToMany
  @JoinTable(
      name = "tbl_tester_device",
      joinColumns = @JoinColumn(name = "tester_id"),
      inverseJoinColumns = @JoinColumn(name = "device_id"))
  private List<Device> devices = new ArrayList<>();

  @Setter(AccessLevel.NONE)
  @OneToMany(mappedBy = "tester", cascade = CascadeType.ALL)
  private List<Bug> bugs = new ArrayList<>();


  public Tester addDevice(Device device) {
    devices.add(device);
    return this;
  }

  public Tester addBug(Bug bug) {
    bugs.add(bug.setTester(this));
    return this;
  }
}
