package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Accessors(chain = true)
@Getter
@Setter
@Entity
@Table(name = "tbl_device")
public class Device {
  @Id
  private Long id;

  @Column(nullable = false)
  private String description;
}
