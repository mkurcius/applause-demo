package com.example.demo.api.search;

import com.example.demo.api.search.dto.CriteriaDTO;
import com.example.demo.api.search.dto.CriteriaValuesDTO;
import com.example.demo.api.search.dto.TesterDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class TesterController {
  private final TesterDAO testerDAO;

  public TesterController(TesterDAO testerDAO) {
    this.testerDAO = testerDAO;
  }

  @GetMapping("/testers/criteria")
  public CriteriaValuesDTO searchParamsValues() {
    return testerDAO.getCriteriaValues();
  }

  // powinno być również zaimplementowane stronicowanie,
  // ale z uwagi, że jest to demo zostało to pominięte
  @GetMapping("/testers")
  public List<TesterDTO> search(
      @RequestParam(name = "country", required = false) Set<String> countries,
      @RequestParam(name = "device", required = false) Set<Long> devices) {
    return testerDAO.findTesters(new CriteriaDTO(countries, devices));
  }
}
