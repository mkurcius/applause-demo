package com.example.demo.api.search;

import com.example.demo.domain.Device;
import com.example.demo.domain.Device_;
import com.example.demo.domain.Tester;
import com.example.demo.domain.Tester_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.Set;

public class TesterSpecification {
  public static Specification<Tester> inCountry(Set<String> countries) {
    return (root, query, criteriaBuilder) -> root.get(Tester_.country).in(countries);
  }

  public static Specification<Tester> inDevices(Set<Long> devices) {
    return (root, query, criteriaBuilder) -> {
      Subquery<Void> subquery = query.subquery(Void.class);
      Root<Tester> from = subquery.correlate(root);
      ListJoin<Tester, Device> join = from.join(Tester_.devices);
      subquery.where(join.get(Device_.id).in(devices));

      return criteriaBuilder.exists(subquery);
    };
  }
}
