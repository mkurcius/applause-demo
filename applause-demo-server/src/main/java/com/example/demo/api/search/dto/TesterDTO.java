package com.example.demo.api.search.dto;

import lombok.Value;

@Value
public class TesterDTO {
  private long id;
  private String firstName;
  private String lastName;
  private long rank;
}
