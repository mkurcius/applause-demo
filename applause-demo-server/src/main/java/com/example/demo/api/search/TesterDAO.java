package com.example.demo.api.search;

import com.example.demo.api.search.dto.CriteriaDTO;
import com.example.demo.api.search.dto.CriteriaValuesDTO;
import com.example.demo.api.search.dto.DeviceDTO;
import com.example.demo.api.search.dto.TesterDTO;
import com.example.demo.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class TesterDAO {
  private final EntityManager entityManager;

  public TesterDAO(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public CriteriaValuesDTO getCriteriaValues() {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

    CriteriaQuery<String> countries = criteriaBuilder.createQuery(String.class);
    Root<Tester> fromTesters = countries.from(Tester.class);
    countries.select(fromTesters.get(Tester_.country)).distinct(true);

    CriteriaQuery<DeviceDTO> devices = criteriaBuilder.createQuery(DeviceDTO.class);
    Root<Device> fromDevices = devices.from(Device.class);
    devices.select(criteriaBuilder.construct(DeviceDTO.class,
        fromDevices.get(Device_.id),
        fromDevices.get(Device_.description)));

    return CriteriaValuesDTO.builder()
        .countries(entityManager.createQuery(countries).getResultList())
        .devices(entityManager.createQuery(devices).getResultList())
        .build();
  }

  public List<TesterDTO> findTesters(CriteriaDTO params) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<TesterDTO> query = criteriaBuilder.createQuery(TesterDTO.class);
    Root<Tester> root = query.from(Tester.class);
    ListJoin<Tester, Bug> bugs = root.join(Tester_.bugs, JoinType.LEFT);

    Expression<Long> rank = criteriaBuilder.count(bugs);
    query.select(criteriaBuilder.construct(TesterDTO.class,
        root.get(Tester_.id),
        root.get(Tester_.firstName),
        root.get(Tester_.lastName),
        rank));

    buildSpecification(params).ifPresent(specification ->
        query.where(specification.toPredicate(root, query, criteriaBuilder)));

    query.groupBy(root.get(Tester_.id), root.get(Tester_.firstName), root.get(Tester_.lastName));
    query.orderBy(criteriaBuilder.desc(rank));

    return entityManager.createQuery(query).getResultList();
  }

  private Optional<Specification<Tester>> buildSpecification(CriteriaDTO params) {
    List<Specification<Tester>> specifications = new LinkedList<>();
    params.getCountries()
        .map(TesterSpecification::inCountry)
        .ifPresent(specifications::add);
    params.getDevices()
        .map(TesterSpecification::inDevices)
        .ifPresent(specifications::add);

    Specification<Tester> specification = null;
    for (Specification<Tester> spec : specifications) {
      specification = specification != null ? specification.and(spec) : spec;
    }

    return Optional.ofNullable(specification);
  }
}
