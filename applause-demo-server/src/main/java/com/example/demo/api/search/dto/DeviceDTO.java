package com.example.demo.api.search.dto;

import lombok.Value;

@Value
public class DeviceDTO {
  private long id;
  private String label;
}
