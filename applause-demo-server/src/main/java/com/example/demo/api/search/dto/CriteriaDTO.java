package com.example.demo.api.search.dto;

import lombok.Value;

import java.util.Optional;
import java.util.Set;

@Value
public class CriteriaDTO {
  private Set<String> countries;
  private Set<Long> devices;

  public Optional<Set<String>> getCountries() {
    return Optional.ofNullable(countries)
        .filter(it -> !it.isEmpty());
  }

  public Optional<Set<Long>> getDevices() {
    return Optional.ofNullable(devices)
        .filter(it -> !it.isEmpty());
  }
}
