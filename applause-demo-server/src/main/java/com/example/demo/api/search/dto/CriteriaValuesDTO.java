package com.example.demo.api.search.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class CriteriaValuesDTO {
  private List<String> countries;
  private List<DeviceDTO> devices;
}
