import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'testers',
    loadChildren: () => import('./search-testers/search-testers.module').then(m => m.SearchTestersModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'testers',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
