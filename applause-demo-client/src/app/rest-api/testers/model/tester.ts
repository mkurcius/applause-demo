export interface Tester {
  id: number
  firstName: string
  lastName: string
  rank: number
}
