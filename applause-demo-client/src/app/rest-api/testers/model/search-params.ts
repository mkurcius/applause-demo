export interface SearchParams {
  countries?: string[]
  devices?: number[]
}
