export interface Device {
  id: number
  label: string
}
