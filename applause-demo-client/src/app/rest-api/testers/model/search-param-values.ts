import { Device } from './device';

export interface SearchParamValues {
  countries: string[]
  devices: Device[]
}
