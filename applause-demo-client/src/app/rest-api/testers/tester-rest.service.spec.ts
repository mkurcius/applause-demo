import { TestBed } from '@angular/core/testing';

import { TesterRestService } from './tester-rest.service';

describe('TesterRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TesterRestService = TestBed.get(TesterRestService);
    expect(service).toBeTruthy();
  });
});
