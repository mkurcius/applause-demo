import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchParamValues } from './model/search-param-values';
import { SearchParams } from './model/search-params';
import { Tester } from './model/tester';

@Injectable({
  providedIn: 'root',
})
export class TesterRestService {
  constructor(private http: HttpClient) { }

  getValues(): Observable<SearchParamValues> {
    return this.http.get<SearchParamValues>('/api/testers/criteria');
  }

  search(params: SearchParams): Observable<Tester[]> {
    const queryParams: {
      country?: string[],
      device?: string[]
    } = {};

    if (params.countries) {
      queryParams.country = params.countries;
    }
    if (params.devices) {
      queryParams.device = params.devices.map(String);
    }


    return this.http.get<Tester[]>('/api/testers', {
      params: queryParams,
    });
  }
}
