import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { CriteriaModule } from './criteria/criteria.module';
import { ResultsComponent } from './results/results.component';

import { SearchTestersRoutingModule } from './search-testers-routing.module';
import { SearchTestersComponent } from './search-testers.component';


@NgModule({
  imports: [
    CommonModule,
    SearchTestersRoutingModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    CriteriaModule,
    MatListModule,
  ],
  declarations: [
    SearchTestersComponent,
    ResultsComponent,
  ],
})
export class SearchTestersModule {}
