import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTestersComponent } from './search-testers.component';

describe('SearchTestersComponent', () => {
  let component: SearchTestersComponent;
  let fixture: ComponentFixture<SearchTestersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchTestersComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTestersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
