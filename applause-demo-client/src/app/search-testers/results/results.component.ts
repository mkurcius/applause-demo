import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { Tester } from '../../rest-api/testers/model/tester';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultsComponent {
  @Input()
  testers: Tester[];
  @Output()
  selected = new EventEmitter<Tester[]>();

  handleChange(event: MatSelectionListChange) {
    const testers: Tester[] = event.source.options
      .filter(opt => opt.selected)
      .map(opt => opt.value);

    this.selected.emit(testers);
  }
}
