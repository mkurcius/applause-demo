import { TestBed } from '@angular/core/testing';

import { SearchTestersService } from './search-testers.service';

describe('SearchTestersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchTestersService = TestBed.get(SearchTestersService);
    expect(service).toBeTruthy();
  });
});
