import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchParamValues } from '../rest-api/testers/model/search-param-values';
import { SearchParams } from '../rest-api/testers/model/search-params';
import { Tester } from '../rest-api/testers/model/tester';
import { SearchTestersService } from './search-testers.service';

@Component({
  selector: 'app-search-testers',
  templateUrl: './search-testers.component.html',
  styleUrls: ['./search-testers.component.scss'],
  providers: [SearchTestersService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchTestersComponent implements OnInit {
  constructor(private service: SearchTestersService) { }

  searchParamValuesLoaded$: Observable<boolean>;
  searchParamValues$: Observable<SearchParamValues>;

  testersLoaded$: Observable<boolean>;
  testersLoading$: Observable<boolean>;
  testers$: Observable<Tester[]>;

  ngOnInit() {
    this.searchParamValuesLoaded$ = this.service.searchParamValuesLoaded$;
    this.searchParamValues$ = this.service.searchParamValues$;

    this.testersLoaded$ = this.service.testersLoaded$;
    this.testersLoading$ = this.service.testersLoading$;
    this.testers$ = this.service.testers$;

    this.service.loadValues();
    this.service.searchTesters();
  }

  handleChanged(params: SearchParams) {
    this.service.searchTesters(params);
  }

  handleSelected(tester: Tester) {
    console.log('selected', tester);
  }
}
