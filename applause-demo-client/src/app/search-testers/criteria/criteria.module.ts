import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { ContainsPipe } from './contains.pipe';
import { CriteriaComponent } from './criteria.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatIconModule,
  ],
  declarations: [
    CriteriaComponent,
    ContainsPipe,
  ],
  exports: [
    CriteriaComponent,
  ],
})
export class CriteriaModule {}
