import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Device } from '../../rest-api/testers/model/device';
import { SearchParamValues } from '../../rest-api/testers/model/search-param-values';
import { SearchParams } from '../../rest-api/testers/model/search-params';

@Component({
  selector: 'app-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CriteriaComponent implements OnInit, OnDestroy {
  @Input()
  values: SearchParamValues;
  @Output()
  changed = new EventEmitter<SearchParams>();

  form: FormGroup;
  private sub: Subscription;

  constructor(private fb: FormBuilder) { }


  ngOnInit(): void {
    this.form = this.fb.group({
      countries: this.fb.control([]),
      devices: this.fb.control([]),
    });

    this.sub = this.form.valueChanges
      .pipe(map(formValue => CriteriaComponent.mapToSearch(formValue)))
      .subscribe(params => this.changed.emit(params));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  toggleCountry(country: string) {
    const countriesForm = this.form.get('countries') as FormControl;
    let countries = countriesForm.value as string[];
    const idx = countries.indexOf(country);

    if (idx >= 0) {
      countries = countries.filter(item => item !== country);
      countriesForm.setValue(countries);
    } else {
      countries = [...countries, country];
      countriesForm.setValue(countries);
    }
  }

  toggleDevice(device: Device) {
    const devicesForm = this.form.get('devices') as FormControl;
    let devices = devicesForm.value as number[];
    const idx = devices.indexOf(device.id);

    if (idx >= 0) {
      devices = devices.filter(item => item !== device.id);
      devicesForm.setValue(devices);
    } else {
      devices = [...devices, device.id];
      devicesForm.setValue(devices);
    }
  }

  private static mapToSearch(formValue: any): SearchParams {
    const params: SearchParams = {};

    if (formValue.countries.length > 0) {
      params.countries = formValue.countries;
    }

    if (formValue.devices.length > 0) {
      params.devices = formValue.devices;
    }

    return params;
  }
}
