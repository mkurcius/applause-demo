import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contains',
})
export class ContainsPipe<T> implements PipeTransform {
  transform(values: T[], item: T): boolean {
    return values.indexOf(item) >= 0;
  }
}
