import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchTestersComponent } from './search-testers.component';

const routes: Routes = [
  {path: '', component: SearchTestersComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchTestersRoutingModule {}
