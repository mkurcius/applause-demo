import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, merge, noop, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, debounceTime, switchMap, tap } from 'rxjs/operators';
import { SearchParamValues } from '../rest-api/testers/model/search-param-values';
import { SearchParams } from '../rest-api/testers/model/search-params';
import { Tester } from '../rest-api/testers/model/tester';
import { TesterRestService } from '../rest-api/testers/tester-rest.service';

@Injectable()
export class SearchTestersService implements OnDestroy {
  private sub: Subscription;
  private searchParamValuesLoaded = new BehaviorSubject<boolean>(false);
  private searchParamValues = new BehaviorSubject<SearchParamValues>(null);
  private testersLoaded = new BehaviorSubject<boolean>(false);
  private testersLoading = new BehaviorSubject<boolean>(false);
  private testers = new BehaviorSubject<Tester[]>([]);

  private loadValuesAction = new Subject();
  private searchTestersAction = new Subject<SearchParams>();

  readonly searchParamValuesLoaded$: Observable<boolean> = this.searchParamValuesLoaded.asObservable();
  readonly searchParamValues$: Observable<SearchParamValues> = this.searchParamValues.asObservable();

  readonly testersLoaded$: Observable<boolean> = this.testersLoaded.asObservable();
  readonly testersLoading$: Observable<boolean> = this.testersLoading.asObservable();
  readonly testers$: Observable<Tester[]> = this.testers.asObservable();

  constructor(private testerRestService: TesterRestService) {
    this.sub = merge(
      this.createLoadValuesAction(),
      this.createSearchTestersAction(),
    ).subscribe(noop);
  }

  loadValues() {
    this.loadValuesAction.next();
  }

  searchTesters(params: SearchParams = {}) {
    this.searchTestersAction.next(params);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


  // ACTIONS
  private createLoadValuesAction(): Observable<unknown> {
    return this.loadValuesAction.pipe(
      switchMap(() => this.testerRestService.getValues().pipe(
        tap(values => {
          this.searchParamValues.next(values);
          this.searchParamValuesLoaded.next(true);
        }),
        catchError(() => of()))),
    );
  }

  private createSearchTestersAction(): Observable<unknown> {
    return this.searchTestersAction.pipe(
      tap(() => this.testersLoading.next(true)),
      debounceTime(500),
      switchMap(params => this.testerRestService.search(params).pipe(
        tap(testers => {
          this.testers.next(testers);
          this.testersLoading.next(false);
          this.testersLoaded.next(true);
        }),
        catchError(() => of()),
      )),
    );
  }
}
